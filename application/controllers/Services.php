<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// The controller to handle AJAX requests.
// Responses are JSON data rather than "views" in a normal sense, so we
// just emit the data from within the controller.
class Services extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->output->set_content_type('application/json');
        $this->load->model('Polls_model');
    }
    /**
     * Send out a JSON-encoded representation of a list of polls
     * @param int $pollId poll
     */
    public function polls($pollId=NULL)
    {
        $method = $this->input->method(TRUE);        
        if ($pollId == NULL && $method == 'GET'){
            try {
            $data = $this->Polls_model->get_pollsJSON();
            $this->output->set_output(json_encode($data));
            } catch (Exception $ex) {
                $this->output->set_status_header(404, 'Not Found');
                show_404();
            }
        } else if ($pollId !== NULL && $method == 'GET'){
            try {
                $data = $this->Polls_model->get_pollJSON($pollId);
                $this->output->set_output(json_encode($data));
            } catch (Exception $ex) {
                $this->output->set_status_header(404, 'Not Found');
                show_404();
            }             
        } else {
            $this->output->set_status_header(404, 'Not Found');
        }        
    }
    /**
     * Send out a JSON-encoded representation of the votes via GET, or adds a
     * vote via POST, or rests a specified polls votes via DELETE.
     * @param int $pollId Poll ID
     * @param int $vote Answer ID
     */
    public function votes($pollId, $vote=NULL){
        $method = $this->input->method(TRUE);        
        if ($vote == NULL && $method == 'GET'){
            try {
            $data = $this->Polls_model->getNumVotesJSON($pollId);
            $this->output->set_output(json_encode($data));
            } catch (Exception $ex) {
                $this->output->set_status_header(404, 'Not Found');
                show_404();
            }
        } else if ($vote !== NULL && $method == 'POST'){
            try {
                $ipAddress = $this->input->ip_address();
                $this->Polls_model->addVote($pollId, $vote, $ipAddress);
            } catch (Exception $ex) {
                $this->output->set_status_header(404, 'Not Found');
            }
        } else if ($vote == NULL && $method == 'DELETE'){
            try{
                $this->Polls_model->resetVotes($pollId);
            } catch (Exception $ex) {
                $this->output->set_status_header(404, 'Not Found');
            }
            
        } else {
            $this->output->set_status_header(404, 'Not Found');
        }       
    }
}
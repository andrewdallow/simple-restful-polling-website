<?php

class Poll extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }
    /**
     * Loads the front page of the website polls.php
     */
    public function view(){
        $this->load->view('index');
    }
}
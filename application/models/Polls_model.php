<?php
/**
 * Polls_model class used for accessing the tables accociated with the polls
 */
class Polls_model extends CI_Model
{
       
    public function __construct() {
        $this->load->database();
    }
    /**
     * Quary the poll database for the poll details used in the JSON response
     * @param int $pollId ID if poll
     * @return associative array of poll details. 
     */
    public function get_pollJSON($pollId){
        $polls = new Polls_model();
        $query = $this->db->get_where('poll', array('id'=> $pollId));
        
        if ($query->num_rows() !== 1) {
            throw new Exception("Poll ID $pollId not found in database");
        }
        
        $row = $query->row_array();
        
        
        
        $poll = array(
            'id' => $row['id'],
            'title' => $row['title'],
            'question' => $row['question'],
            'answers' => $polls->listAllAnswers($pollId)
        );
        
        return $poll;
    }
    /**
     * Quary the database for a list of all polls in the poll table giving 
     * the poll title and question.
     * @return list if polls
     */
    public function get_pollsJSON(){
        
        $poll = array();
        
        $quary = $this->db->get('poll');
        $rows = $quary->result();
        
        foreach ($rows as $row){
            $poll[] = array(
                'id' => $row->id,
                'title' => $row->title,
                'question' => $row->question
            );
        }
        return $poll;
        
    }
    /**
     * Quary the database for a list of all answers in a given poll ID
     * @param int $pollId the ID of the poll
     * @return list of all poll answers
     */
    public function listAllAnswers($pollId){
        $list = array();
        
        $quary = $this->db->get_where('pollanswers', array('pollid'=> $pollId));
        $rows = $quary->result_array();
        
        foreach ($rows as $row) {
            $list[] = array('id' => $row['id'], 'answer' => $row['answer']);
        }        
        return $list;       
        
    }
    
    /**
     * For a given poll, count the number of votes for each answer.
     * @param int $pollId The poll ID
     * @return $votes list of votes for each answer.
     */
    public function getNumVotesJSON($pollId){
        $list = array();
        
        $this->db->select('pollanswers.id AS id, pollanswers.answer AS answer, '
                . 'COALESCE(COUNT(vote.answerid),0) AS numVotes', FALSE);
        
        $this->db->join('vote', 'vote.answerid=pollanswers.id', 'LEFT', FALSE);
        
        $this->db->where('pollanswers.pollid', $pollId, FALSE);
        $this->db->group_by('pollanswers.answer', FALSE);
        $this->db->order_by("id");
        
        $rows = $this->db->get('pollanswers')->result();
        if (empty($rows)) {
            throw new Exception("Poll ID $pollId not found in database");
        }
        
        foreach ($rows as $row){
            $list[] = array(
                'answer' => $row->answer,
                'total' => $row->numVotes
                );
        }
        
        $votes['votes'] = $list;
        return $votes;
    }
    /**
     * Checks if an answer for a given poll is in the database. 
     * @param int $pollId Poll ID
     * @param type $vote Answer ID
     * @return boolean True if valid False if not. 
     */
    public function isValidAnswer($pollId, $vote){
        $query = $this->db->get_where('pollanswers', array('id'=> $vote, 'pollid' => $pollId));        
        if ($query->num_rows() == 1) {
            return TRUE;
        }
        return FALSE;        
    }
    /**
     * Add a vote to the vote database
     * @param int $pollId poll ID
     * @param int $vote Answer ID
     * @param String $ipAddress Users IP address
     * @throws Exception on invalid vote
     */
    public function addVote($pollId, $vote, $ipAddress){
        $poll = new Polls_model();
        if ($poll->isValidAnswer($pollId, $vote)){
            $data = array(
                'pollid' => $pollId,
                'ipaddress' => $ipAddress,
                'answerid' => $vote
            );
            $this->db->insert('vote', $data);
        } else {
            throw new Exception("Invalid Vote");
        }
        
    }
    /**
     * Delete all votes for a give poll
     * @param int $pollId poll ID
     */
    public function resetVotes($pollId){
        $query = $this->db->get_where('poll', array('id'=> $pollId));
        
        if ($query->num_rows() !== 1) {
            throw new Exception("Poll ID $pollId not found in database");
        }
        
        $this->db->delete('vote', array('pollid' => $pollId));
        
    }
}
    
            
 
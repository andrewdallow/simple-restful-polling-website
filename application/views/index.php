<!DOCTYPE html>
<html lang="en" ng-app="pollsApp">
<head>
  <meta charset="utf-8">
  <title>Polls</title>
  <link rel="stylesheet" type="text/css" href="style/styles.css">
  
  <script src="scripts/angular.js"></script>
  <script src="scripts/angular-route.js"></script>
  <script src="js/app.js"></script>
  <script src="js/controllers.js"></script>
</head>
<body>
    <div class='container'>
        <nav>
            <table class='nav-bar'>
                <tr>
                    <td class='nav-bar'><a href="#/polls">Poll List</a></td>
                    <td class='nav-bar'><a href="#/about">About Page</a></td>
                </tr>
            </table>

        </nav>

        <div ng-view></div>
    </div>

</body>
<footer class="container">    
    <p>Andrew Dallow 56999204</p>
</footer>
</html>
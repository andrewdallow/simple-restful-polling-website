(function () {
    'use strict';

    /* App Module */

    var pollsApp = angular.module('pollsApp', [
      'ngRoute',
      'pollsControllers'
    ]);
    //Routes to the three views
    pollsApp.config(['$routeProvider',
      function($routeProvider) {
        $routeProvider.
          when('/polls', {
            templateUrl: 'partials/poll-list.html',
            controller: 'PollsCtrl'
          }).
          when('/about', {
            templateUrl: 'partials/about.html',
            controller: 'AboutCtrl'
          }).
          when('/polls/:pollId', {
            templateUrl: 'partials/poll-detail.html',
            controller: 'PollDetailCtrl'
          }).
          otherwise({
            redirectTo: '/polls'
          });
      }]);
}());

(function () {
    'use strict';

    /* Controllers */
    var pollsControllers = angular.module('pollsControllers', []);

    /**
     * Controller for poll-list view which fetches a list of all polls in
     * the database.
     */
    pollsControllers.controller('PollsCtrl', ['$scope', '$http',
        function ($scope, $http) {
            $http.get('index.php/services/polls').success(function(data){
                $scope.polls = data;
            });
        }]);
    
    /**
     * Controller for the poll-detail view, which fetches the details of a 
     * selected poll and also keeps track of the currently selected answer,
     * which wen submitted, is added to the vote table. Also has the ability to
     * view the vote results and reset votes. 
     */
    pollsControllers.controller('PollDetailCtrl', ['$scope', '$routeParams', '$http',
      function($scope, $routeParams, $http) {
            var pollId = $routeParams.pollId;
            $scope.success_msg = undefined;
            $scope.choice = undefined;

            $http.get('index.php/services/polls/' + pollId).success(function(data){
                $scope.poll = data;
              });
            //function controlling the submit button
            $scope.submit = function () {
              $http.post('index.php/services/votes/' + pollId + '/' 
                      + $scope.choice).success(function(){
              $scope.success_msg = 'Your vote has been submited. Thank you for voting.';
              $scope.choice = undefined;
            });
            };
            //Function for viewing the vote results table
            $scope.voteResults = function () {
                $http.get('index.php/services/votes/' + pollId).success(function(data){
                $scope.votes = data.votes;
              });
            };
            //function for reseting the votes for the selected poll.
            $scope.reset = function () {
                $http.delete('index.php/services/votes/' + pollId).success(function(){
                $scope.success_msg = 'Votes for this poll have been reset.';
                $scope.voteResults();
              });
            };
      }]);
    /**
     * Controller of the about page. 
     */
    pollsControllers.controller('AboutCtrl', ['$scope']);
    
  }());